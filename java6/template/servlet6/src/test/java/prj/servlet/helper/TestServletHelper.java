package prj.servlet.helper;

import static org.junit.Assert.*;

import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.client.ContentExchange;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import ch.gmtech.fields.Fields;
import prj.data.HttpMethodEnum;
import prj.servlet.Servlet;

public class TestServletHelper {
	
	public static Server startHttpServer(int serverPort) throws Exception {
		Server server = new Server(serverPort);
		ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		handler.setContextPath("/");
		server.setHandler(handler);
		handler.addServlet(new ServletHolder(new Servlet()),"/*");
		server.start();
		return server;
	}
	
	public static void assert404(String host, int port, String path) throws Exception {
		ContentExchange response = get(host, port, path);
		assertEquals(404, response.getResponseStatus());
		assertTrue(response.getResponseContent().contains("custom 404 message"));
	}
	
	public static ContentExchange get(String host, int port, String path) throws Exception {
		HttpClient client = new HttpClient();
		client.start();
		ContentExchange exchange = new ContentExchange(true);
		exchange.setURL("http://" + host + ":" + port + path);
		 
		client.send(exchange);
		exchange.waitForDone();
		return exchange;
	}
	
	public static ContentExchange post(String host, int port, String path, Fields httpParameters) throws Exception {
		StringBuilder paramsBuffer = new StringBuilder();
		for (String eachKey : httpParameters) {
			paramsBuffer.append(eachKey+"="+URLEncoder.encode(httpParameters.firstValueFor(eachKey), "UTF-8")+"&");
		}
		String params = StringUtils.removeEnd(paramsBuffer.toString(), "&");
		String url = "http://" + host + ":" + port + path + "?" + params;
		
		HttpClient client = new HttpClient();
		client.start();
		ContentExchange exchange = new ContentExchange(true);
		exchange.setURL(url);
		exchange.setMethod(HttpMethodEnum.POST.toString());
		
		client.send(exchange);
		exchange.waitForDone();
		return exchange;
	}

	

}
