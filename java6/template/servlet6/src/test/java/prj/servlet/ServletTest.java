package prj.servlet;

import static org.junit.Assert.*;
import static prj.servlet.helper.TestServletHelper.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.client.ContentExchange;
import org.eclipse.jetty.server.Server;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.gmtech.data.Data;
import ch.gmtech.data.DatePatterns;
import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.connection.DbConnection;
import ch.gmtech.persistence.util.DbUtils;
import prj.data.PeristenceTableData;
import prj.resource.course.Course;
import prj.resource.student.Student;

public class ServletTest {
	
	private Server _server;
	private int _serverPort;
	private Course _seminar;
	private String _host;

	@Before
	public void setUp() throws Exception {
		_host = "localhost";
		_serverPort = 8079;
		_server = startHttpServer(_serverPort);
		Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse("2016-10-29 20:00:00");
		_seminar = new Course(1, "java for dummies", "learn java", "mendrisio", 23, startDate);
		_seminar.addStudent(new Student("Joey", "Ramone"));
		_seminar.addStudent(new Student("Dee Dee", "Ramone"));
	}

	@After
	public void tearDown() throws Exception {
		DbUtils.truncate(PeristenceTableData.COURSE, DbConnection.Factory.test());
		_server.stop();
	}
	
	@Test
	public void getCourses() throws Exception {
		ContentExchange response = get(_host, _serverPort, "/course/");
		assertEquals(HttpServletResponse.SC_OK, response.getResponseStatus());
		
		String body = response.getResponseContent();
		assertTrue(body.contains(">seminar list</h1>"));
		assertTrue(body.contains("<table class"));
		assertTrue(body.contains("<tbody></tbody>"));
	}
	@Test
	public void getFormForCourseCreate() throws Exception {
		ContentExchange response = get(_host, _serverPort, "/course/create");
		assertEquals(HttpServletResponse.SC_OK, response.getResponseStatus());
		assertTrue(response.getResponseContent().contains("<form "));
	}
	
	@Test
	public void createACourse() throws Exception {
		Fields courseParameters = Fields.empty()
				.put(Data.NAME, "aName")
				.put(Data.LOCATION, "aLocation")
				.put(Data.NUMBER_OF_SEATS, "123")
				.put(Data.START_DATE, "2017-02-20 20:00:00")
				.put(Data.DESCRIPTION, "aDescription");
		ContentExchange createResponse = post(_host, _serverPort, "/course/create", courseParameters);
		assertEquals(HttpServletResponse.SC_FOUND, createResponse.getResponseStatus());
		
		ContentExchange response = get(_host, _serverPort, "/course/");
		System.out.println("qui => " + response.getResponseContent());
		assertTrue(response.getResponseContent().contains("<td>aName</td>"));
	}
	

	@Test
	public void removedRoutes() throws Exception {
		assert404("localhost", 8079, "/try/me");
		assert404("localhost", 8079, "/course/html");
		assert404("localhost", 8079, "/course/csv");
		assert404("localhost", 8079, "/course/raw");
	}
	
}
