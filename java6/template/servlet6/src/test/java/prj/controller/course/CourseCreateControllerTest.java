package prj.controller.course;

import static org.junit.Assert.*;

import java.io.StringWriter;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.gmtech.context.Context;
import ch.gmtech.context.FakeHttpRequest;
import ch.gmtech.context.FakeHttpResponse;
import ch.gmtech.data.Data;
import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.AbstractPersistence;
import ch.gmtech.persistence.connection.AbstractDbConnection;
import ch.gmtech.persistence.connection.DbConnection;
import prj.controller.helper.UrlHelper;
import prj.data.HttpMethodEnum;
import prj.factory.persistence.PersistenceFactory;
import prj.view.helper.HtmlHelper;

public class CourseCreateControllerTest {
	
	private Fields _parameters;
	private AbstractPersistence _repository;
	private CourseCreateController _controller;
	private AbstractDbConnection _dbConnection;

	@Before
	public void setUp() throws SQLException {
		_parameters = Fields.empty()
			.put(Data.NAME, "aName")
			.put(Data.ID, "23")
			.put(Data.LOCATION, "aLocation")
			.put(Data.NUMBER_OF_SEATS, "123")
			.put(Data.DESCRIPTION, "aDescription")
			.put(Data.START_DATE, "2017-01-01 14:00:00");
		_dbConnection = DbConnection.Factory.test();
		_repository = PersistenceFactory.course(_dbConnection);
		_controller = new CourseCreateController(_repository);
	}
	
	@After
	public void tearDown() throws SQLException {
		_dbConnection.rollback();
		_dbConnection.close();
	}

	@Test
	public void createOK() throws Exception {
		FakeHttpRequest request = new FakeHttpRequest(HttpMethodEnum.GET, "/seminar/create", _parameters);
		FakeHttpResponse response = new FakeHttpResponse();
		Context context = new Context(request, response);
		
		_controller.applyOn(context);
		
		assertEquals(1, _repository.countAll());
		assertEquals(UrlHelper.list(Data.COURSE), response.getRedirectLocation());
	}

	@Test
	public void createNameIsMandatory() throws Exception {
		_parameters.remove(Data.NAME);
		FakeHttpRequest _request = new FakeHttpRequest(HttpMethodEnum.GET, "/seminar/create", _parameters);
		StringWriter out = new StringWriter();
		FakeHttpResponse response = new FakeHttpResponse("", out);
		Context context = new Context(_request, response);
		
		_controller.applyOn(context);
		
		assertEquals(0, _repository.countAll());
		
		assertEquals(1, context.getResourceErrors(Data.COURSE).size());
		assertEquals(Fields.single(Data.NAME, "name is mandatory").toString(), context.getResourceErrors(Data.COURSE).toString());
		
		assertTrue(out.toString().contains(HtmlHelper.inputTextFor(Data.NAME, context).toText()));
	}
	
	@Test
	public void createCheckNumberOfSeatsValidation() throws Exception {
		_parameters.remove(Data.NUMBER_OF_SEATS);
		FakeHttpRequest _request = new FakeHttpRequest(HttpMethodEnum.GET, "/seminar/create", _parameters);
		StringWriter out = new StringWriter();
		FakeHttpResponse response = new FakeHttpResponse("", out);
		Context context = new Context(_request, response);
		
		_controller.applyOn(context);
		
		assertEquals(0, _repository.countAll());
		
		assertEquals(1, context.getResourceErrors(Data.COURSE).size());
		assertEquals(Fields.single(Data.NUMBER_OF_SEATS, "numberOfSeats is mandatory,numberOfSeats must be numeric").toString(), context.getResourceErrors(Data.COURSE).toString());
		
//		assertTrue(out.toString().contains(HtmlHelper.inputTextFor(Data.NUMBER_OF_SEATS, context).toText()));
	}

}
