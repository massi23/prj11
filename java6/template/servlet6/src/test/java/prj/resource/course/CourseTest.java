package prj.resource.course;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import ch.gmtech.data.DatePatterns;
import prj.resource.course.report.CourseCSVReportStrategy;
import prj.resource.course.report.CourseHTMLReportStrategy;
import prj.resource.student.Student;

public class CourseTest  {

	@Test
	public void testSeminarUiIntegration() throws Exception {
		Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse("2016-10-29 20:00:00");
		Course seminar = new Course(1, "java for dummies", "learn java", "mendrisio", 23, startDate);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		assertEquals(
				"course: java for dummies - 1\n"
				+ "description: learn java\n"
				+ "location: mendrisio\n"
				+ "seats left: 23\n"
				+ "start date: 2016-10-29 20:00:00\n"
				+ "students:\n"
				+ "\tJoey Ramone"
				+ "\tDee Dee Ramone", seminar.report());
	}
	
	@Test
	public void testSeminarDetailsAsHTML() throws Exception {
		Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse("2016-10-29 20:00:00");
		Course seminar = new Course(1, "java for dummies", "learn java", "mendrisio", 23, startDate);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		
		String expected = 
				"<html>"+
				"<head>"+
				"     <title>nome corso</title>"+
				"</head> "+
				"<body>"+
				"    <div>java for dummies</div>"+
				"    <ul>"+
				"          <li>learn java</li>"+
				"          <li>mendrisio</li>"+
				"          <li>23</li>"+
				"          <li>2016-10-29 20:00:00</li>"+
				"    </ul>"+
				"    <div>partecipanti:</div>"+
				"    <ul>"+
				"          <li>Joey Ramone</li>"+
				"          <li>Dee Dee Ramone</li>"+
				"    </ul>"+
				"</body>"+
				"</html>";
		assertEquals(expected, seminar.report(new CourseHTMLReportStrategy()));
	}
	
	@Test
	public void testSeminarDetailsAsCSV() throws Exception {
		Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse("2016-10-29 20:00:00");
		Course seminar = new Course(1, "java for dummies", "learn java", "mendrisio", 23, startDate);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		String expected =
				"\"1\";\"java for dummies\";\"learn java\";\"mendrisio\";\"23\";\"2016-10-29 20:00:00\";\n"
				+ "\"Joey\";\"Ramone\";\n"
				+ "\"Dee Dee\";\"Ramone\";\n";
		assertEquals(expected, seminar.report(new CourseCSVReportStrategy()));
	}
	
}
