package ch.gmtech.persistence;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.gmtech.data.Data;
import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.connection.DbConnection;
import prj.factory.persistence.PersistenceFactory;

public class SqlPersistenceTest {

	private DbConnection _connection;
	private AbstractPersistence _repo;

	@Before
	public void setUp() throws Exception {
		_connection = DbConnection.Factory.test();
		_repo = PersistenceFactory.course(_connection);
	}
	@After
	public void tearDown() throws Exception  {
		_connection.rollback();
		_connection.close();
	}
	
	@Test
	public void testSaveAndFind() {
		Fields toSave = Fields.single(Data.NAME, "punk seminar")
			.put(Data.NUMBER_OF_SEATS, "10")
			.put(Data.DESCRIPTION, "bla bla")
			.put(Data.LOCATION, "Mendrisio")
			.put(Data.START_DATE, "2017-06-01 00:00:00");
		
		assertEquals(0, _repo.countAll());
		_repo.save(toSave);
		assertEquals(1, _repo.countAll());

		Fields expected = Fields.single(Data.ID, "1");
		expected.put(toSave);
		assertEquals(expected.toString(), _repo.findAll().get(0).toString());
	}

}
