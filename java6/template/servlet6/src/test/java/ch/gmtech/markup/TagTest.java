package ch.gmtech.markup;

import static j2html.TagCreator.*;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class TagTest {

	@Test
	public void test() {
		Tag tag = Tag.containerTag("html").withChildren(
				Tag.containerTag("head").withChildren(
				Tag.containerTag("title").withText("Title"),
				Tag.emptyTag("link").withAttribute("rel", "stylesheet").withAttribute("href", "/css/main.css")
			),
			Tag.containerTag("body").withChildren(
					Tag.containerTag("main").withAttribute("class", "container").withChildren(
							Tag.containerTag("h1").withText("Heading!")
//							,Tag.containerTag("h2").withText("Heading 2" + Tag.containerTag("small").withText("xxx").toText())
					)
			)
		);
		assertEquals(expectedPage(), tag.toText());
	}


	private String expectedPage() {
		return html().with(
			    head().with(
			            title("Title"),
			            link().withRel("stylesheet").withHref("/css/main.css")
			        )
			    ,
			        body().with(
			            main().withClass("container").with(
			                h1("Heading!")
			            )
			        )
			    ).render();
	}

}
