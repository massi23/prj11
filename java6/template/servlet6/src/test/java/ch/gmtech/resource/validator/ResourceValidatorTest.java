package ch.gmtech.resource.validator;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import ch.gmtech.data.Data;
import ch.gmtech.data.DatePatterns;
import ch.gmtech.fields.Fields;

public class ResourceValidatorTest {

	@Test
	public void testPresenceOf_OK() {
		ResourceValidator validator = new ResourceValidator();
		validator.validatePresenceOf(Data.NAME);
		
		assertTrue(validator.isValid(Fields.single(Data.NAME, "homer")));
		assertTrue(validator.errors().isEmpty());
	}

	@Test
	public void testPresenceOf_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validatePresenceOf(Data.NAME);
		
		assertFalse(validator.isValid(Fields.empty()));
		assertEquals(Data.NAME + " is mandatory", validator.errors().firstValueFor(Data.NAME));
	}
	
	@Test
	public void testNumericalityOf_OK() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateNumericalityOf(Data.ID);
		
		assertTrue(validator.isValid(Fields.single(Data.ID, "23")));
		assertTrue(validator.errors().isEmpty());
	}
	
	@Test
	public void testNumericalityOf_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateNumericalityOf(Data.ID);
		
		assertFalse(validator.isValid(Fields.empty()));
		assertEquals(Data.ID + " must be numeric", validator.errors().firstValueFor(Data.ID));
	}
	
	@Test
	public void testDateFormatOf_OK() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateDateFormatOf(Data.START_DATE, DatePatterns.DEFAULT);
		
		assertTrue(validator.isValid(Fields.single(Data.START_DATE, "2017-01-23 23:23:23")));
		assertTrue(validator.errors().isEmpty());
	}
	
	@Test
	public void testDateFormatOf_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateDateFormatOf(Data.START_DATE, DatePatterns.DEFAULT);
		
		assertFalse(validator.isValid(Fields.single(Data.START_DATE, "23.23.2017 23:23:23")));
		assertEquals(Data.START_DATE + " has a wrong format. the supported one is '" + DatePatterns.DEFAULT + "'", validator.errors().firstValueFor(Data.START_DATE));
	}
	
	@Test
	public void testMaxLengthOf_OK() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateMaxLengthOf(Data.NAME, 5);
		
		assertTrue(validator.isValid(Fields.single(Data.NAME, "12345")));
		assertTrue(validator.errors().isEmpty());
	}
	
	@Test
	public void testMaxLengthOf_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateMaxLengthOf(Data.NAME, 5);
		
		assertFalse(validator.isValid(Fields.single(Data.NAME, "123456")));
		assertEquals(Data.NAME + " is too long. max length is 5", validator.errors().firstValueFor(Data.NAME));
	}
	
	@Test
	public void testMaxValueOf_OK() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateMaxValueOf(Data.NUMBER_OF_SEATS, 5);
		
		assertTrue(validator.isValid(Fields.single(Data.NUMBER_OF_SEATS, "5")));
		assertTrue(validator.errors().isEmpty());
	}
	
	@Test
	public void testMaxValueOf_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validateMaxValueOf(Data.NUMBER_OF_SEATS, 5);
		
		assertFalse(validator.isValid(Fields.single(Data.NUMBER_OF_SEATS, "6")));
		assertEquals(Data.NUMBER_OF_SEATS + " value is too high. max value is 5", validator.errors().firstValueFor(Data.NUMBER_OF_SEATS));
	}
	
	@Test
	public void testManytErrors_KO() {
		ResourceValidator validator = new ResourceValidator();
		validator.validatePresenceOf(Data.NUMBER_OF_SEATS);
		validator.validateNumericalityOf(Data.NUMBER_OF_SEATS);
		String [] expectedErrors = {
				Data.NUMBER_OF_SEATS + " is mandatory",
				Data.NUMBER_OF_SEATS + " must be numeric"
		};
		
		assertFalse(validator.isValid(Fields.empty()));
		
		List<String> errors = validator.errors().allValuesFor(Data.NUMBER_OF_SEATS);
		for (String expected : expectedErrors) {
			assertTrue(expected + " not found", errors.contains(expected));
		}
	}

}
