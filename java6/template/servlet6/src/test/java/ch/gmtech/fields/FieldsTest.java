package ch.gmtech.fields;

import static org.junit.Assert.*;

import org.junit.Test;

public class FieldsTest {

	@Test
	public void testPut() {
		Fields fields = Fields.empty();

		assertFalse(fields.contains("aKey", "aValue"));
		
		fields.put("aKey", "aValue");
		assertTrue(fields.contains("aKey", "aValue"));
		assertFalse(fields.contains("aKey", "anotherValue"));
	}
	
	@Test
	public void testPutShouldReplaceValues() {
		Fields fields = Fields.empty().put("aKey", "aValue");

		fields.put("aKey", "anotherValue");
		assertFalse(fields.contains("aKey", "aValue"));
		assertTrue(fields.contains("aKey", "anotherValue"));
	}
	
	@Test
	public void testPutemptyValues() {
		Fields fields = Fields.empty();

		assertFalse(fields.containsKey("aKey"));
		
		fields.put("aKey", "");
		assertFalse(fields.contains("aKey", ""));
	}
	
	@Test
	public void testAdd() {
		Fields fields = Fields.empty().put("aKey", "aValue");
		fields.add("aKey", "anotherValue");
		
		assertTrue(fields.contains("aKey", "aValue"));
		assertTrue(fields.contains("aKey", "anotherValue"));
		assertFalse(fields.contains("aKey", "unknownValue"));
	}
	
	@Test
	public void testSelect() {
		Fields fields = Fields.empty().put("person:name", "mario");
		fields.add("person:name", "andrea");
		fields.add("person:surname", "rossi");
		fields.add("address:city", "lugano");
		fields.add("address:street", "via cantonale");
		
		Fields result = fields.selectByPrefix("address:");
		assertEquals(2, result.size());
		assertTrue(result.contains("address:city", "lugano"));
		assertTrue(result.contains("address:street", "via cantonale"));
		
		result = fields.selectByPrefix("person:");
		assertEquals(2, result.size());
		assertTrue(result.contains("person:name", "mario"));
		assertTrue(result.contains("person:name", "andrea"));
		assertTrue(result.contains("person:surname", "rossi"));
	}

}
