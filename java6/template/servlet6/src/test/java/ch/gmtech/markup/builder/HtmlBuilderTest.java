package ch.gmtech.markup.builder;

import static j2html.TagCreator.*;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import ch.gmtech.markup.Tag;

@Ignore
public class HtmlBuilderTest {

	@Test
	public void testEmptyBuilder() throws Exception {
		assertEquals("", new HtmlBuilder().build());
	}

	@Test
	public void testHtmlSkeletonPage() throws Exception {
		String result = new HtmlBuilder()
				.withHtml()
				.withHead()
				.withBody()
				.build();
		assertEquals("<html><head></head><body></body></html>", result);
	}

	@Test
	public void testPage() throws Exception {
		String result = new HtmlBuilder()
			.withHtml()
			.withHead(
					Tag.containerTag("title").withText("Title"),
					Tag.emptyTag("link").withAttribute("rel", "stylesheet").withAttribute("href", "/css/main.css")
			)
			.withBody(
					Tag.containerTag("main").withAttribute("class", "container").withChildren(
							Tag.containerTag("h1").withText("Heading!")
					)
			)
			.build()
		;
		assertEquals(expectedPage(), result);
	}

	@Test
	public void testFragment() throws Exception {
		String result = new HtmlBuilder()
			.withHtml()
			.withHead()
			.withBody()
			.with(
					Tag.containerTag("main").withAttribute("class", "container").withChildren(
							Tag.containerTag("h1").withText("Heading!")
					)
			)
			.build()
		;
		assertEquals("<main class=\"container\"><h1>Heading!</h1></main>", result);
	}

	private String expectedPage() {
		return html().with(
			    head().with(
			            title("Title"),
			            link().withRel("stylesheet").withHref("/css/main.css")
			        )
			    ,
			        body().with(
			            main().withClass("container").with(
			                h1("Heading!")
			            )
			        )
			    ).render();
	}

}
