package ch.gmtech.context.recogniser.helper;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.gmtech.context.Context;
import ch.gmtech.context.FakeHttpRequest;
import ch.gmtech.context.FakeHttpResponse;
import ch.gmtech.context.recogniser.AbstractContextRecogniser;

public class ResourceRecogniserTest {

	@Test
	public void show() {
		AbstractContextRecogniser showRecogniser = ResourceRecogniser.list("seminar");
		assertTrue(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminar/"), new FakeHttpResponse())));
		assertTrue(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminar"), new FakeHttpResponse())));
		assertFalse(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminarIO"), new FakeHttpResponse())));
	}
	
	@Test
	public void createForm() {
		AbstractContextRecogniser showRecogniser = ResourceRecogniser.createForm("seminar");
		assertTrue(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminar/create"), new FakeHttpResponse())));
		assertTrue(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminar/create/"), new FakeHttpResponse())));
		assertFalse(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminarcreate/"), new FakeHttpResponse())));
		assertFalse(showRecogniser.recognise(new Context(new FakeHttpRequest("/seminarcreate"), new FakeHttpResponse())));
	}

}
