package ch.gmtech.context;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ch.gmtech.fields.Fields;
import prj.data.HttpMethodEnum;

public class ContextTest {

	private Context _context;

	@Before
	public void setUp() {
		Fields parameters = Fields.empty()
			.put("name", "aName")
			.put("number", "23")
			.put("location", "aLocation")
			.put("numberOfSeats", "123")
			.put("descrition", "aDescription");
		FakeHttpRequest request = new FakeHttpRequest(HttpMethodEnum.GET, "/test-path", parameters);
		
		_context = new Context(request, new FakeHttpResponse());
	}

	@Test
	public void testMethodMatch() {
		assertTrue("http method recognition failed", _context.matchesMethod(HttpMethodEnum.GET));
		assertFalse("http method recognition failed", _context.matchesMethod(HttpMethodEnum.POST));
	}
	
	@Test
	public void testPathMatch() {
		assertTrue("should recognise path '/test-path'", _context.matchesPath("/test-path"));
		assertFalse("should NOT recognise path '/wrong-path'", _context.matchesPath("/wrong-path"));
	}
	
	@Test
	public void testParameters() {
		Fields expected = Fields.empty()
			.put("name", "aName")
			.put("number", "23")
			.put("location", "aLocation")
			.put("numberOfSeats", "123")
			.put("descrition", "aDescription");
		Fields result = _context.getParameters();
		
		for (String eachKey : expected) {
			assertEquals("should extract parameter '" + eachKey + "'", expected.firstValueFor(eachKey), result.firstValueFor(eachKey));
		}
	}

}
