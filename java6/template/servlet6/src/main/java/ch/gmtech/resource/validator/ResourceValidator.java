package ch.gmtech.resource.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.fields.Fields;

public class ResourceValidator {

	private final List<String> _mandatoryKeys;
	private final List<String> _numericValues;
	private final Map<String, String> _dateFormats;
	private final Map<String, Integer> _maxLengthMappings;
	private final Map<String, Integer> _maxValueMappings;
	private final Fields _errors;
	
	public ResourceValidator() {
		_mandatoryKeys = new ArrayList<String>();
		_numericValues = new ArrayList<String>();
		_dateFormats = new HashMap<String, String>();
		_maxLengthMappings = new HashMap<String, Integer>();
		_maxValueMappings = new HashMap<String, Integer>();
		_errors = Fields.empty();
	}

	public boolean isValid(Fields someFields) {
		boolean isMandatoryOk = checkMandatoryFor(someFields);
		boolean isNumericalityOk = checkNumericalityFor(someFields);
		boolean isDateFormatsOk = checkDateFotmatsFor(someFields);
		boolean areMaxLengthsOk = checkMaxLengthsFor(someFields);
		boolean areMaxValuesOk = checkMaxValueFor(someFields);
		return isMandatoryOk && isNumericalityOk && isDateFormatsOk && areMaxLengthsOk && areMaxValuesOk;
	}

	public void validatePresenceOf(String aKey) {
		_mandatoryKeys.add(aKey);
	}
	
	public void validateNumericalityOf(String aKey) {
		_numericValues.add(aKey);
	}
	
	public void validateDateFormatOf(String aKey, String aPattern) {
		_dateFormats.put(aKey, aPattern);
	}
	
	public void validateMaxLengthOf(String key, int length) {
		_maxLengthMappings.put(key, length);
	}
	
	public void validateMaxValueOf(String key, int value) {
		_maxValueMappings.put(key, value);
	}
	
	public Fields errors() {
		return _errors;
	}


	private boolean checkMaxValueFor(Fields someFields) {
		boolean result = true;
		for (String key : _maxValueMappings.keySet()) {
			Integer maxValue = _maxValueMappings.get(key);
			int currentValue = Integer.parseInt(someFields.firstValueFor(key, "0"));
			if(currentValue <= maxValue) continue;
			result = false;
			addError(key, key + " value is too high. max value is " + maxValue);
		}
		return result;
	}
	
	private boolean checkMaxLengthsFor(Fields someFields) {
		boolean result = true;
		for (String key : _maxLengthMappings.keySet()) {
			Integer maxLength = _maxLengthMappings.get(key);
			if(someFields.firstValueFor(key).length() <= maxLength) continue;
			result = false;
			addError(key, key + " is too long. max length is " + maxLength);
		}
		return result;
	}

	private boolean checkMandatoryFor(Fields someFields) {
		boolean result = true;
		for (String key : _mandatoryKeys) {
			if(someFields.containsKey(key)) continue;
			result = false;
			addError(key, key + " is mandatory");
		}
		return result;
	}
	
	private boolean checkNumericalityFor(Fields someFields) {
		boolean result = true;
		for (String key : _numericValues) {
			if(StringUtils.isNumeric(someFields.firstValueFor(key))) continue;
			result = false;
			addError(key, key + " must be numeric");
		}
		return result;
	}
	
	private boolean checkDateFotmatsFor(Fields someFields) {
		boolean result = true;
		for (String key : _dateFormats.keySet()) {
			String format = _dateFormats.get(key);
			SimpleDateFormat parser = new SimpleDateFormat(format);
			try {
				parser.parse(someFields.firstValueFor(key));
			} catch (ParseException e) {
				result = false;
				addError(key, key + " has a wrong format. the supported one is '" + format + "'");
			}
		}
		return result;
	}

	private void addError(String key, String message) {
		_errors.add(key, message);
	}
}
