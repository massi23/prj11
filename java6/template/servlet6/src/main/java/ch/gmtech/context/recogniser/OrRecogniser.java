package ch.gmtech.context.recogniser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.context.Context;

public class OrRecogniser extends AbstractContextRecogniser {

	private final List<AbstractContextRecogniser> _recognisers;
	
	public OrRecogniser() {
		_recognisers = new ArrayList<AbstractContextRecogniser>();
	}

	@Override
	public boolean recognise(Context aContext) {
		for (AbstractContextRecogniser eachRecogniser : _recognisers) {
			if(eachRecogniser.recognise(aContext)) return true;
		}
		return false;
	}

	public OrRecogniser add(AbstractContextRecogniser aRecogniser) {
		_recognisers.add(aRecogniser);
		return this;
	}

	@Override
	public String description() {
		StringBuilder result = new StringBuilder();
		for (AbstractContextRecogniser recogniser : _recognisers) {
			result.append(recogniser.description() + " or ");
		}
		return "(" +  StringUtils.removeEnd(result.toString(), " or ") + ")";
	}

}
