package ch.gmtech.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.connection.AbstractDbConnection;

public class SqlPersistence extends AbstractPersistence {

	private final AbstractDbConnection _connection;
	private final String _tableName;
	private final List<String> _columns;

	public SqlPersistence(AbstractDbConnection aDbConnection, String tableName, List<String> columns) {
		_connection = aDbConnection;
		_tableName = tableName;
		_columns = columns;
	}

	@Override
	public void save(Fields toSave) {
		try {
			String[] keys = toSave.keys();
			String sql = sql(keys);
			PreparedStatement statement = _connection.prepareStatement(sql);
			fillStatement(toSave, keys, statement);
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Fields> findAll() {
		try {
			ArrayList<Fields> result = new ArrayList<Fields>();
			String sql = "select * from " + _tableName;
			PreparedStatement statement = _connection.prepareStatement(sql);
			ResultSet set = statement.executeQuery();
			System.out.println(sql);
			while (set.next()) {
				Fields record = Fields.empty();
				for (String key : _columns) {
					record.put(key, set.getString(key));
				}
				System.out.println(record);
				result.add(record);
			}
			statement.close();
			return result;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int countAll() {
		int result = 0;
		try {
			PreparedStatement statement = _connection.prepareStatement("select count(*) as myCount from " + _tableName);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			result = resultSet.getInt("myCount");
			statement.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
	
	private void fillStatement(Fields toSave, String[] keys, PreparedStatement statement) throws SQLException {
		int index = 1;
		for (String key : keys) {
			statement.setObject(index, toSave.firstValueFor(key));
			index++;
		}
	}

	private String sql(String[] keys) {
		StringBuilder sql = new StringBuilder();
		sql.append("insert into ");
		sql.append(_tableName);
		sql.append(" (");
		sql.append(StringUtils.join(keys, ", "));
		sql.append(") values (");
		sql.append(placeholdersFor(keys));
		sql.append(")");
		return sql.toString();
	}

	private String placeholdersFor(String[] keys) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < keys.length; i++) {
			result.append("?,");
		}
		return StringUtils.removeEnd(result.toString(), ",");
	}

}
