package ch.gmtech.persistence.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import ch.gmtech.persistence.connection.DbConnection;

public class DbUtils {

	public static void truncate(String table, DbConnection connection) throws SQLException {
		String sql = "delete from " + table;
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.executeUpdate();
		preparedStatement.close();
		connection.commit();
		connection.close();
	}

}
