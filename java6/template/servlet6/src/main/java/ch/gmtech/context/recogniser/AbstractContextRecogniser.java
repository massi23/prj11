package ch.gmtech.context.recogniser;

import ch.gmtech.context.Context;


public abstract class AbstractContextRecogniser {

	public abstract boolean recognise(Context aContext);
	public abstract String description();

}
