package ch.gmtech.persistence.connection;

import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.data.EnvironmentData;

public class DbConnectionFactory {

	private DataSource _dataSource;

	public DbConnectionFactory(String environment) throws NamingException {
		if(StringUtils.equals(environment, EnvironmentData.PRODUCTION)) {
			 _dataSource = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/ds");
		}
	}

	public DbConnection create() throws SQLException {
		if(_dataSource == null) {
			return DbConnection.Factory.test();
		}
		return DbConnection.Factory.create(_dataSource);
	}

}
