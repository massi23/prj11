package ch.gmtech.context.recogniser;

import ch.gmtech.context.Context;
import prj.data.HttpMethodEnum;

public class ByHttpMethodRecogniser extends AbstractContextRecogniser {

	private final HttpMethodEnum _httpMethod;

	public ByHttpMethodRecogniser(HttpMethodEnum httpMethod) {
		_httpMethod = httpMethod;
	}

	@Override
	public boolean recognise(Context aContext) {
		return aContext.matchesMethod(_httpMethod);
	}

	@Override
	public String description() {
		return "http method is " + _httpMethod.toString();
	}

}
