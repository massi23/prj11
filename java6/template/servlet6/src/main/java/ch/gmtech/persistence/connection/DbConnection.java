package ch.gmtech.persistence.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

public class DbConnection extends AbstractDbConnection {

	private final Connection _connection;

	public static class Factory {

		public static DbConnection test() throws SQLException {
			Connection connection = DriverManager.getConnection("jdbc:sqlite:src/test/resources/sql/seminar.db");
			connection.setAutoCommit(false);
			return new DbConnection(connection);
		}

		public static DbConnection create(DataSource ds) throws SQLException {
			Connection connection = ds.getConnection();
			connection.setAutoCommit(false);
			return new DbConnection(connection);
		}
		
	}
	
	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return _connection.prepareStatement(sql);
	}
	@Override
	public void commit() throws SQLException {
		_connection.commit();
	}
	@Override
	public void rollback() throws SQLException {
		_connection.rollback();
	}
	@Override
	public void close() {
		try {
			_connection.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (_connection != null) {
		        try {
		        	_connection.close();
		        } catch (SQLException e) {}
			}
		}
	}

	private DbConnection(Connection connection) {
		_connection = connection;
	}

}
