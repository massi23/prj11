package ch.gmtech.context;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.gmtech.fields.Fields;
import prj.data.HttpMethodEnum;

public class Context {


	private final HttpServletRequest _request;
	private final HttpServletResponse _response;
	private final Map<String, Fields> _resourceErrors;
	private final List<String> _errors;
	private final Fields _defaultRequestParameters;

	public Context(HttpServletRequest request, HttpServletResponse response) {
		_request = request;
		_response = response;
		_resourceErrors = new HashMap<String, Fields>();
		_errors = new ArrayList<String>();
		_defaultRequestParameters = Fields.empty();
	}

	public boolean matchesMethod(HttpMethodEnum anHttpMethod) {
		return _request.getMethod().equals(anHttpMethod.toString());
	}

	public boolean matchesPath(String aPath) {
		return _request.getRequestURI().equals(aPath);
	}


	public void writeOut(String someText) throws Exception {
		_response.getWriter().write(someText);
	}


	public void setResponseCodeTo(int anHttpResponseCode, String aMessage) throws Exception {
		_response.sendError(anHttpResponseCode, aMessage);
	}

	public Fields getParameters() {
		Fields result = _defaultRequestParameters.copy();
		Enumeration<String> parameterNames = _request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String eachKey = parameterNames.nextElement();
			String[] values = _request.getParameterValues(eachKey);
			for (String eachValue : values) {
				result.add(eachKey, eachValue);
			}
		}
		return result;
	}

	public void putDefaultRequestParameter(String aKey, String aValue) {
		_defaultRequestParameters.put(aKey, aValue);
	}

	public void redirectTo(String aPath) throws Exception {
		_response.sendRedirect(aPath);
	}

	public void putResourceError(String aResource, Fields someErrors) {
		_resourceErrors.put(aResource, someErrors);
	}

	public Fields getResourceErrors(String aResource) {
		if(!_resourceErrors.containsKey(aResource)) return Fields.empty();
		return _resourceErrors.get(aResource);
	}

	public void addError(String error) {
		_errors.add(error);
	}

	public List<String> getErrors() {
		return _errors;
	}

	public String description() {
		return _request.toString();
	}

}
