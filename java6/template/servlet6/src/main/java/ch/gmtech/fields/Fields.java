package ch.gmtech.fields;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Fields implements Iterable<String> {

	private final LinkedHashMap<String, List<String>> _data;

	public static Fields empty() {
		return new Fields();
	}
	public static Fields single(String key, String value) {
		return empty().put(key, value);
	}

	public Fields put(String aKey, String aValue) {
		if(StringUtils.isEmpty(aValue)) return this;
		List<String> values = new ArrayList<String>();
		values.add(aValue);
		_data.put(aKey, values);
		return this;
	}

	public void put(Fields fields) {
		for (String key : fields._data.keySet()) {
			put(key, fields.allValuesFor(key));
		}
	}
	
	public Fields add(String aKey, String aValue) {
		if(!containsKey(aKey)) return put(aKey, aValue);
		_data.get(aKey).add(aValue);
		return this;
	}

	public String firstValueFor(String aKey) {
		return firstValueFor(aKey, "");
	}
	
	public String firstValueFor(String aKey, String defaultValue) {
		if(!containsKey(aKey)) return defaultValue;
		return _data.get(aKey).get(0);
	}
	
	public List<String> allValuesFor(String aKey) {
		if(!containsKey(aKey)) return new ArrayList<String>();
		return new ArrayList<String>(_data.get(aKey));
	}

	public boolean contains(String aKey, String aValue) {
		if(!containsKey(aKey)) return false;
		List<String> values = _data.get(aKey);
		for (String each : values) {
			if(each.equals(aValue)) return true;
		}
		return false;
	}
	
	public boolean containsKey(String aKey) {
		return _data.containsKey(aKey);
	}
	
	public void remove(String aKey) {
		_data.remove(aKey);
	}
	
	public Fields selectByPrefix(String aPrefix) {
		Fields result = Fields.empty();
		for (String key : _data.keySet()) {
			if(key.startsWith(aPrefix)) {
				result.put(key, allValuesFor(key));
			}
		}
		return result;
	}

	
	public int size() {
		return _data.size();
	}
	
	public boolean isEmpty() {
		return _data.isEmpty();
	}
	
	public Fields copy() {
		Fields result = Fields.empty();
		for (String each : this) {
			result.put(each, allValuesFor(each));
		}
		return result;
	}
	
	public String[] keys() {
		return _data.keySet().toArray(new String [] {});
	}
	
	@Override
	public Iterator<String> iterator() {
		return _data.keySet().iterator();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (String key : _data.keySet()) {
			builder.append(key+"="+valuesToStingFor(key) + "|");
		}
		return StringUtils.removeEnd(builder.toString(), "|");
	}

	
	private void put(String aKey, List<String> someValues) {
		_data.put(aKey, someValues);
	}

	private String valuesToStingFor(String key) {
		StringBuilder builder = new StringBuilder();
		for (String value : allValuesFor(key)) {
			builder.append(value + ",");
		}
		return StringUtils.removeEnd(builder.toString(), ",");
	}

	private Fields() {
		_data = new LinkedHashMap<String, List<String>>();
	}
	
}
