package ch.gmtech.context.recogniser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.context.Context;

public class AndRecogniser extends AbstractContextRecogniser {

	private final List<AbstractContextRecogniser> _recognisers;
	
	public AndRecogniser() {
		_recognisers = new ArrayList<AbstractContextRecogniser>();
	}

	@Override
	public boolean recognise(Context aContext) {
		for (AbstractContextRecogniser each : _recognisers) {
			if(!each.recognise(aContext)) return false;
		}
		return true;
	}

	public AndRecogniser add(AbstractContextRecogniser aRecogniser) {
		_recognisers.add(aRecogniser);
		return this;
	}

	@Override
	public String description() {
		StringBuilder result = new StringBuilder();
		for (AbstractContextRecogniser recogniser : _recognisers) {
			result.append(recogniser.description() + " and ");
		}
		return "(" + StringUtils.removeEnd(result.toString(), " and ") + ")";
	}

}
