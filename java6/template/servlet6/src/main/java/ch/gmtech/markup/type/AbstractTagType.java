package ch.gmtech.markup.type;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.markup.Tag;

public abstract class AbstractTagType {

	private final String _name;
	private final Map<String, String> _attributes;

	public AbstractTagType(String aName) {
		_name = aName;
		_attributes = new HashMap<String, String>();
	}

	public abstract void setText(String aText);
	public abstract void addChildren(List<Tag> someTags);
	public abstract String toText();

	protected String name() {
		return _name;
	}

	public void putAttribute(String aName, String aValue) {
		_attributes.put(aName, aValue);
	}

	protected String attributesToText() {
		if(_attributes.isEmpty()) return "";
		StringBuilder builder = new StringBuilder();
		builder.append(" ");
		for (String each : _attributes.keySet()) {
			builder.append(each + "=\"" + _attributes.get(each) +"\" ");
		}
		return StringUtils.removeEnd(builder.toString(), " ");
	}

}
