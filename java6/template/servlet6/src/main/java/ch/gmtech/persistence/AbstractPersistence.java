package ch.gmtech.persistence;

import java.util.List;

import ch.gmtech.fields.Fields;

public abstract class AbstractPersistence {

	public abstract void save(Fields record);
	public abstract List<Fields> findAll();
	public abstract int countAll();

}
