package ch.gmtech.logger;

import java.io.PrintStream;

public class PrintStreamLogger extends AbstractLogger {
	
    private final PrintStream _stream;
    
    public PrintStreamLogger(PrintStream stream) {
        _stream = stream;
    }
    
    @Override
    public void log(String value) {
        _stream.println(value);
    }

}
