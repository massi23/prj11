package ch.gmtech.markup;

import java.util.Arrays;

import ch.gmtech.markup.type.AbstractTagType;
import ch.gmtech.markup.type.ContainerTagType;
import ch.gmtech.markup.type.EmptyTagType;
import ch.gmtech.markup.type.NullContainerTagType;

public class Tag {

	private final AbstractTagType _tagType;

	public static Tag containerTag(String aName) {
		return new Tag(new ContainerTagType(aName));
	}

	public static Tag emptyTag(String aName) {
		return new Tag(new EmptyTagType(aName));
	}

	public static Tag nullTag() {
		return new Tag(new NullContainerTagType());
	}


	public Tag withChildren(Tag...children) {
		_tagType.addChildren(Arrays.asList(children));
		return this;
	}

	public Tag withText(String aText) {
		_tagType.setText(aText);
		return this;
	}

	public Tag withAttribute(String aName, String aValue) {
		_tagType.putAttribute(aName, aValue);
		return this;
	}

	public String toText() {
		return _tagType.toText();
	}

	private Tag(AbstractTagType tagType) {
		_tagType = tagType;
	}
}
