package ch.gmtech.data;

public class Data {

	public static final String ENVIRONMENT = "environment";
	public static final String COURSE = "course";
	public static final String NAME = "name";
	public static final String START_DATE = "startDate";
	public static final String LOCATION = "location";
	public static final String NUMBER_OF_SEATS = "numberOfSeats";
	public static final String ID = "id";
	public static final String DESCRIPTION = "description";

}
