package ch.gmtech.context.recogniser;

import ch.gmtech.context.Context;


public class ByPathRequestRecogniser extends AbstractContextRecogniser{

	private final String _path;

	public ByPathRequestRecogniser(String path) {
		_path = path;
	}

	@Override
	public boolean recognise(Context aContext) {
		return aContext.matchesPath(_path);
	}

	@Override
	public String description() {
		return "path matches " + _path;
	}

}
