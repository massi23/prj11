package ch.gmtech.markup.type;

import java.util.ArrayList;
import java.util.List;

import ch.gmtech.markup.Tag;

public class ContainerTagType extends AbstractTagType {

	private String _text;
	private List<Tag> _children;

	public ContainerTagType(String aName) {
		super(aName);
		_text = "";
		_children = new ArrayList<Tag>();
	}

	@Override
	public void setText(String aText) {
		_text = aText;
	}

	@Override
	public void addChildren(List<Tag> someTags) {
		_children = someTags;
	}

	@Override
	public String toText() {
		return "<" + name() +attributesToText()+ ">"+_text+childrenToText()+"</"+name()+">";
	}

	private String childrenToText() {
		StringBuilder builder = new StringBuilder();
		for (Tag each : _children) {
			builder.append(each.toText());
		}
		return builder.toString();
	}


}
