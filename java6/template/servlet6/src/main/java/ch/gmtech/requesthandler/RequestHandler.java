package ch.gmtech.requesthandler;

import java.util.HashMap;
import java.util.Map;

import ch.gmtech.context.Context;
import ch.gmtech.context.recogniser.AbstractContextRecogniser;
import ch.gmtech.logger.AbstractLogger;
import prj.controller.AbstractController;

public class RequestHandler {


	private final Map<AbstractContextRecogniser, AbstractController> _controllers;
	private final AbstractController _defaultResponse;
	private final AbstractLogger _logger;

	public RequestHandler(AbstractLogger logger, AbstractController defaultResponse) {
		_logger = logger;
		_defaultResponse = defaultResponse;
		_controllers = new HashMap<AbstractContextRecogniser, AbstractController>();
	}

	public void put(AbstractContextRecogniser aRecogniser, AbstractController aResponse) {
		_controllers.put(aRecogniser, aResponse);
	}

	public void handle(Context context) throws Exception {
		for (AbstractContextRecogniser recogniser : _controllers.keySet()) {
			if(recogniser.recognise(context)) {
				_logger.log("context " + context.description() + " recognised by " + recogniser.description());
				_controllers.get(recogniser).applyOn(context);
				return;
			}

		}
		_logger.log("request not recognised");
		_defaultResponse.applyOn(context);
	}

}
