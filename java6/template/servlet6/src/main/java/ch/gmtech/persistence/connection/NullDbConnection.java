package ch.gmtech.persistence.connection;

import java.sql.PreparedStatement;

public class NullDbConnection extends AbstractDbConnection {

	@Override
	public PreparedStatement prepareStatement(String aSql) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void commit() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void rollback() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void close() {
	}

}
