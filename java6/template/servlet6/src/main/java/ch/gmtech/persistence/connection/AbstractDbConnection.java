package ch.gmtech.persistence.connection;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractDbConnection {

	public abstract PreparedStatement prepareStatement(String sql) throws SQLException;
	public abstract void commit() throws SQLException;
	public abstract void rollback() throws SQLException;
	public abstract void close();

}
