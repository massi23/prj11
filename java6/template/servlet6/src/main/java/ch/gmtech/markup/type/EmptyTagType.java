package ch.gmtech.markup.type;

import java.util.List;

import ch.gmtech.markup.Tag;

public class EmptyTagType extends AbstractTagType {

	public EmptyTagType(String aName) {
		super(aName);
	}

	@Override
	public void setText(String aText) {}
	@Override
	public void addChildren(List<Tag> someTags) {}
	@Override
	public String toText() {
		return "<" + name() +attributesToText()+ ">";
	}

}
