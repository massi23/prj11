package ch.gmtech.markup.type;

import java.util.ArrayList;
import java.util.List;

import ch.gmtech.markup.Tag;

public class NullContainerTagType extends AbstractTagType {

	private List<Tag> _children;

	public NullContainerTagType() {
		super("");
		_children = new ArrayList<Tag>();
	}

	@Override
	public void setText(String aText) {
	}

	@Override
	public void addChildren(List<Tag> someTags) {
		_children = someTags;
	}

	@Override
	public String toText() {
		StringBuilder builder = new StringBuilder();
		for (Tag each : _children) {
			builder.append(each.toText());
		}
		return builder.toString();
	}

}
