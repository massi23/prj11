package ch.gmtech.context.recogniser.helper;

import ch.gmtech.context.recogniser.AbstractContextRecogniser;
import ch.gmtech.context.recogniser.AndRecogniser;
import ch.gmtech.context.recogniser.ByHttpMethodRecogniser;
import ch.gmtech.context.recogniser.ByPathRequestRecogniser;
import ch.gmtech.context.recogniser.OrRecogniser;
import prj.controller.helper.UrlHelper;
import prj.data.HttpMethodEnum;

public class ResourceRecogniser {
	
	public static AbstractContextRecogniser root() {
		return new AndRecogniser()
				.add(new ByHttpMethodRecogniser(HttpMethodEnum.GET))
				.add(new ByPathRequestRecogniser("/"));
	}

	public static AbstractContextRecogniser list(String aResource) {
		return new AndRecogniser()
					.add(new ByHttpMethodRecogniser(HttpMethodEnum.GET))
					.add(new OrRecogniser()
								.add(new ByPathRequestRecogniser(UrlHelper.list(aResource) + "/"))
								.add(new ByPathRequestRecogniser(UrlHelper.list(aResource)))
					);
	}

	public static AbstractContextRecogniser createForm(String aResource) {
		return new AndRecogniser()
					.add(new ByHttpMethodRecogniser(HttpMethodEnum.GET))
					.add(new OrRecogniser()
								.add(new ByPathRequestRecogniser(UrlHelper.create(aResource) + "/"))
								.add(new ByPathRequestRecogniser(UrlHelper.create(aResource)))
					);
	}

	public static AbstractContextRecogniser create(String aResource) {
		return new AndRecogniser()
					.add(new ByHttpMethodRecogniser(HttpMethodEnum.POST))
					.add(new OrRecogniser()
								.add(new ByPathRequestRecogniser(UrlHelper.create(aResource) + "/"))
								.add(new ByPathRequestRecogniser(UrlHelper.create(aResource)))
					);
	}

}
