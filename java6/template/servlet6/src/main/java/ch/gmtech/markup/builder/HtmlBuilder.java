package ch.gmtech.markup.builder;

import ch.gmtech.markup.Tag;

public class HtmlBuilder {

	private Tag _htmlTag;
	private Tag _headTag;
	private Tag _bodyTag;

	public HtmlBuilder() {
		initToNullTag();
	}

	public HtmlBuilder withHtml() {
		_htmlTag = Tag.containerTag("html");
		return this;
	}

	public HtmlBuilder withHead() {
		_headTag = Tag.containerTag("head");
		return this;
	}

	public HtmlBuilder withHead(Tag ...someTags) {
		_headTag = Tag.containerTag("head").withChildren(someTags);
		return this;
	}

	public HtmlBuilder withBody(Tag ... someTags) {
		_bodyTag = Tag.containerTag("body").withChildren(someTags);
		return this;
	}

	public String build() {
		return _htmlTag.withChildren(_headTag, _bodyTag).toText();
	}

	public HtmlBuilder with(Tag...someTags) {
		initToNullTag();
		_bodyTag.withChildren(someTags);
		return this;
	}

	private void initToNullTag() {
		_htmlTag = Tag.nullTag();
		_headTag = Tag.nullTag();
		_bodyTag = Tag.nullTag();
	}

}
