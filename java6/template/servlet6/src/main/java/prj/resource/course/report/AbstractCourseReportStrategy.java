package prj.resource.course.report;

import prj.resource.course.Course;

public abstract class AbstractCourseReportStrategy {

	public abstract String reportFor(Course aSeminar);

}
