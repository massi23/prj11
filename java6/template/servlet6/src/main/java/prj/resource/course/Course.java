package prj.resource.course;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import prj.resource.course.report.AbstractCourseReportStrategy;
import prj.resource.course.report.CourseTextReportStrategy;
import prj.resource.student.Student;

public class Course {
	
	private final int _id;
	private final String _name;
	private final int _seatsLeft;
	private final String _description;
	private final String _location;
	private final List<Student> _enrollmentList;
	private final Date _startDate;

	public Course(int id, String name, String description, String localtion, int seats, Date startDate) {
		_id = id;
		_name = name;
		_description = description;
		_location = localtion;
		_seatsLeft = seats;
		_startDate = startDate;
		_enrollmentList = new ArrayList<Student>();
	}

	public void addStudent(Student aStudent) {
		_enrollmentList.add(aStudent);
	}

	public String getName() {
		return _name;
	}
	
	public int getId() {
		return _id;
	}

	public String getDescription() {
		return _description;
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatsLeft() {
		return _seatsLeft;
	}

	public List<Student> getStudentList() {
		return new ArrayList<Student>(_enrollmentList);
	}
	
	public String report() {
		return report(new CourseTextReportStrategy());
	}

	public String report(AbstractCourseReportStrategy aSeminarReportStrategy) {
		return aSeminarReportStrategy.reportFor(this);
	}

	public Date getStartDate() {
		return new Date(_startDate.getTime());
	}

}
