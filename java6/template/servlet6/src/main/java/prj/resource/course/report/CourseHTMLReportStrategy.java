package prj.resource.course.report;

import java.text.SimpleDateFormat;

import ch.gmtech.data.DatePatterns;
import prj.resource.course.Course;
import prj.resource.student.Student;

public class CourseHTMLReportStrategy extends AbstractCourseReportStrategy{

	private final SimpleDateFormat _dateFormatter;

	public CourseHTMLReportStrategy() {
		_dateFormatter = new SimpleDateFormat(DatePatterns.DEFAULT);
	}
	
	
	@Override
	public String reportFor(Course aSeminar) {
		StringBuilder result = new StringBuilder()
			.append("<html>")
			.append("<head>")
			.append("     <title>nome corso</title>")
			.append("</head> ")
			.append("<body>")
			.append("    <div>" + aSeminar.getName() + "</div>")
			.append("    <ul>")
			.append("          <li>" + aSeminar.getDescription() + "</li>")
			.append("          <li>" + aSeminar.getLocation() + "</li>")
			.append("          <li>" + aSeminar.getSeatsLeft() + "</li>")
			.append("          <li>" + _dateFormatter.format(aSeminar.getStartDate()) + "</li>")
			.append("    </ul>")
			.append("    <div>partecipanti:</div>")
			.append("    <ul>");
		for (Student each : aSeminar.getStudentList()) {
			result.append("          <li>" + each.getName() + " " + each.getSurname() + "</li>");
		}
		
		result.append("    </ul>")
			.append("</body>")
			.append("</html>");
		return result.toString();
	}

}
