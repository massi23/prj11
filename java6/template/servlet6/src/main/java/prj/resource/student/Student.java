package prj.resource.student;

public class Student {

	private final String _surname;
	private final String _name;

	public Student(String name, String surname) {
		_name = name;
		_surname = surname;
	}

	public String getName() {
		return _name;
	}

	public String getSurname() {
		return _surname;
	}

}
