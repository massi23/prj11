package prj.resource.course.report;

import java.text.SimpleDateFormat;

import ch.gmtech.data.DatePatterns;
import prj.resource.course.Course;
import prj.resource.student.Student;

public class CourseCSVReportStrategy extends AbstractCourseReportStrategy {

	private final String _lineSeparator;
	private final SimpleDateFormat _dateFormatter;

	public CourseCSVReportStrategy() {
		_lineSeparator = System.getProperty("line.separator");
		_dateFormatter = new SimpleDateFormat(DatePatterns.DEFAULT);
	}
	
	@Override
	public String reportFor(Course aSeminar) {
		StringBuilder result = new StringBuilder();
		result.append(csvLine(String.valueOf(aSeminar.getId()), aSeminar.getName(), aSeminar.getDescription(), aSeminar.getLocation(), String.valueOf(aSeminar.getSeatsLeft()), _dateFormatter.format(aSeminar.getStartDate())));
		result.append(_lineSeparator);
		for (Student each : aSeminar.getStudentList()) {
			result.append(csvLine(each.getName(), each.getSurname()));
			result.append(_lineSeparator);
		}
		return result.toString();
	}

	private String csvLine(String ... data) {
		StringBuilder result = new StringBuilder();
		for (String each : data) {
			result.append("\"" + each + "\";");
		}
		return result.toString();
	}

}
