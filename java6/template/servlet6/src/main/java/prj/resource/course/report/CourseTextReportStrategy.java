package prj.resource.course.report;

import java.text.SimpleDateFormat;

import ch.gmtech.data.DatePatterns;
import prj.resource.course.Course;
import prj.resource.student.Student;

public class CourseTextReportStrategy extends AbstractCourseReportStrategy {

	private final SimpleDateFormat _dateFormatter;

	public CourseTextReportStrategy() {
		_dateFormatter = new SimpleDateFormat(DatePatterns.DEFAULT);
	}
	
	@Override
	public String reportFor(Course aSeminar) {
		StringBuilder result = new StringBuilder()
			.append("course: " + aSeminar.getName() + " - " + aSeminar.getId() + "\n")
			.append("description: " + aSeminar.getDescription() + "\n")
			.append( "location: " + aSeminar.getLocation() + "\n")
			.append("seats left: " + aSeminar.getSeatsLeft() + "\n")
			.append("start date: " + _dateFormatter.format(aSeminar.getStartDate()) + "\n")
			.append("students:\n");
		for (Student each : aSeminar.getStudentList()) {
			result.append("\t" + each.getName() + " " + each.getSurname());
		}
		return result.toString();
	}

}
