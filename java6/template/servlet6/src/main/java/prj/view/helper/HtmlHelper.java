package prj.view.helper;

import org.apache.commons.lang3.StringUtils;

import ch.gmtech.context.Context;
import ch.gmtech.data.Data;
import ch.gmtech.fields.Fields;
import ch.gmtech.markup.Tag;

public class HtmlHelper {

	public static Tag inputTextFor(String name, Context context) {
		return inputFor("input", name, name, context);
	}
	
	public static Tag inputFor(String type, String name, String text, Context context) {
		String value = context.getParameters().firstValueFor(name);
		Fields errors = context.getResourceErrors(Data.COURSE);
		// mai inserito
		if(!context.getParameters().containsKey(name) && errors.isEmpty()) {
			return Tag.containerTag("div").withAttribute("class", "form-group").withChildren(
					Tag.containerTag("label").withAttribute("for", name).withAttribute("class", "col-sm-2 control-label").withText(text),
					Tag.containerTag("div").withAttribute("class", "col-sm-10").withChildren(
							Tag.containerTag(type)
								.withAttribute("type", "text")
								.withAttribute("class", "form-control")
								.withAttribute("id", name)
								.withAttribute("name", name)
								.withAttribute("placeholder", text)
								.withAttribute("value", value)																
							)
					);
		}
		// inserito correttamente
		if(context.getParameters().containsKey(name) && !errors.containsKey(name)) {
			String describeAreaId = "idHelp_" + name;
			return Tag.containerTag("div").withAttribute("class", "form-group has-success has-feedback").withChildren(
					Tag.containerTag("label").withAttribute("for", name).withAttribute("class", "col-sm-2 control-label").withText(text),
					Tag.containerTag("div").withAttribute("class", "col-sm-10").withChildren(
							Tag.containerTag(type)
								.withAttribute("type", "text")
								.withAttribute("class", "form-control")
								.withAttribute("id", name)
								.withAttribute("name", name)
								.withAttribute("placeholder", text)
								.withAttribute("value", value)																
								.withAttribute("aria-describedby", describeAreaId),
							Tag.containerTag("span").withAttribute("id", describeAreaId).withAttribute("class", "help-block"),
							Tag.containerTag("span").withAttribute("class", "glyphicon form-control-feedback glyphicon-" + "ok").withAttribute("aria-hidden", "true"),
							Tag.containerTag("span").withAttribute("id", describeAreaId).withAttribute("class", "sr-only").withText("(success)")
						)
					);
		}
		// con errore
		if(errors.containsKey(name)) {
			String describeAreaId = "idHelp_" + name;
			String errorMessage = StringUtils.join(errors.allValuesFor(name), ",");
			return Tag.containerTag("div").withAttribute("class", "form-group has-error has-feedback").withChildren(
					Tag.containerTag("label").withAttribute("for", name).withAttribute("class", "col-sm-2 control-label").withText(text),
					Tag.containerTag("div").withAttribute("class", "col-sm-10").withChildren(
							Tag.containerTag(type)
								.withAttribute("type", "text")
								.withAttribute("class", "form-control")
								.withAttribute("id", name)
								.withAttribute("name", name)
								.withAttribute("placeholder", text)
								.withAttribute("value", value)																
								.withAttribute("aria-describedby", describeAreaId),
							Tag.containerTag("span").withAttribute("id", describeAreaId).withAttribute("class", "help-block").withText(errorMessage),
							Tag.containerTag("span").withAttribute("class", "glyphicon form-control-feedback glyphicon-remove").withAttribute("aria-hidden", "true"),
							Tag.containerTag("span").withAttribute("id", describeAreaId).withAttribute("class", "sr-only").withText("(error)")
					)
				);
		}
		return Tag.nullTag();
	}
	

	public static Tag inputAreatFor(String name) {
		return Tag.containerTag("div").withAttribute("class", "form-group").withChildren(
					Tag.containerTag("label").withAttribute("for", name).withAttribute("class", "col-sm-2 control-label").withText(name),
					Tag.containerTag("div").withAttribute("class", "col-sm-10").withChildren(
							Tag.containerTag("textarea")
								.withAttribute("class", "form-control")
								.withAttribute("id", name)
								.withAttribute("name", name)
								.withAttribute("placeholder", name)
								.withAttribute("value", "")																
							)
					);
	}

	
}
