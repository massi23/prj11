package prj.view.template;

import ch.gmtech.data.Data;
import ch.gmtech.markup.Tag;
import prj.controller.helper.UrlHelper;

public class DefaultHtmlTemplate {

	private final String _resource;
	private Tag _content;
	private final String _title;
	private final String _lead;

	public DefaultHtmlTemplate(String resource, String title, String lead) {
		_resource = resource;
		_title = title;
		_lead = lead;
		_content = Tag.nullTag();
	}

	public String toText() {
		Tag tag = Tag.containerTag("html").withChildren(
				Tag.containerTag("head").withChildren(
				Tag.emptyTag("meta").withAttribute("charset", "utf-8"),
				Tag.emptyTag("meta").withAttribute("http-equiv", "X-UA-Compatible").withAttribute("content", "IE=edge"),
				Tag.emptyTag("meta").withAttribute("name", "viewport").withAttribute("content", "width=device-width, initial-scale=1"),
				Tag.containerTag("title").withText(_resource),
				Tag.emptyTag("link").withAttribute("rel", "stylesheet").withAttribute("href", "/css/bootstrap.min.css"),
				Tag.emptyTag("link").withAttribute("rel", "stylesheet").withAttribute("href", "/css/custom.css"),
				Tag.emptyTag("link").withAttribute("rel", "stylesheet").withAttribute("href", "/css/app.css")
			),
			Tag.containerTag("body").withChildren(
				Tag.containerTag("div").withAttribute("class", "navbar navbar-default navbar-fixed-top").withChildren(
					Tag.containerTag("div").withAttribute("class", "container").withChildren(
						Tag.containerTag("div").withAttribute("class", "navbar-header").withChildren(
							Tag.containerTag("a").withAttribute("class", "navbar-brand").withAttribute("href", "/").withText(_resource),
							Tag.containerTag("button").withAttribute("class", "navbar-toggle").withAttribute("type", "button").withAttribute("data-toggle", "collapse").withAttribute("ata-target", "#navbar-main").withChildren(
									Tag.emptyTag("span").withAttribute("class", "icon-bar"),
									Tag.emptyTag("span").withAttribute("class", "icon-bar"),
									Tag.emptyTag("span").withAttribute("class", "icon-bar")
							)
						),
						Tag.containerTag("div").withAttribute("class", "navbar-collapse collapse").withAttribute("id", "navbar-main").withChildren(
							Tag.containerTag("ul").withAttribute("class", "nav navbar-nav navbar-right").withChildren(
								Tag.containerTag("li").withAttribute("class", "dropdown").withChildren(
									Tag.containerTag("a").withAttribute("class", "dropdown-toggle").withAttribute("data-toggle", "dropdown")
													  .withAttribute("href", "#").withAttribute("id", "download").withAttribute("aria-expanded", "false")
													  .withText("Account" + Tag.containerTag("span").withAttribute("class", "caret").toText()),
									Tag.containerTag("ul").withAttribute("class", "dropdown-menu").withAttribute("aria-labelledby", "download").withChildren(
										Tag.containerTag("li").withChildren(
											Tag.containerTag("a").withAttribute("href", "/").withText("settings")
										),
										Tag.containerTag("li").withAttribute("class", "divider"),
										Tag.containerTag("li").withChildren(
											Tag.containerTag("a").withAttribute("href", "/").withText("logout")
										)
									)
								)
							)
						)
					)
				),
				Tag.containerTag("div").withAttribute("class", "container").withChildren(
					Tag.containerTag("div").withAttribute("class", "page-header").withAttribute("id", "banner").withChildren(
						Tag.containerTag("div").withAttribute("class", "row").withChildren(
							Tag.containerTag("div").withAttribute("class", "col-lg-8 col-md-7 col-sm-6").withChildren(
								Tag.containerTag("h1").withText(_title),
								Tag.containerTag("p").withAttribute("class", "lead").withText(_lead)
							)
						),
						Tag.containerTag("div").withAttribute("class", "row").withChildren(
							Tag.containerTag("div").withAttribute("class", "col-lg-2 col-md-2 col-sm-3").withChildren(
								Tag.containerTag("div").withAttribute("class", "list-group table-of-contents").withChildren(
									Tag.containerTag("a").withAttribute("class", "list-group-item").withAttribute("href", UrlHelper.list(Data.COURSE)).withText("list"),
									Tag.containerTag("a").withAttribute("class", "list-group-item").withAttribute("href", UrlHelper.create(Data.COURSE)).withText("create")
								)
							),
							Tag.containerTag("div").withAttribute("class", "col-lg-8 col-md-8 col-sm-9").withChildren(
								_content
							)
						)
					)
				),
				Tag.containerTag("script").withAttribute("rel", "stylesheet").withAttribute("src", "/js/jquery.min.js?v=1.0.0"),
				Tag.containerTag("script").withAttribute("rel", "stylesheet").withAttribute("src", "/js/bootstrap.min.js?v=1.0.0"),
				Tag.containerTag("script").withAttribute("rel", "stylesheet").withAttribute("src", "/js/custom.js?v=1.0.0")
			)
		);
		return tag.toText();
	}
	
	public DefaultHtmlTemplate withContent(Tag aContent) {
		_content = aContent;
		return this;
	}

}
