package prj.view.course;

import java.util.ArrayList;
import java.util.List;

import ch.gmtech.data.Data;
import ch.gmtech.fields.Fields;
import ch.gmtech.markup.Tag;
import prj.view.template.DefaultHtmlTemplate;

public class CourseListView {

	private final List<Fields> _courseList;

	public CourseListView(List<Fields> courseList) {
		_courseList = courseList;
	}

	public String toText() {
		List<Tag> tableRows = new ArrayList<Tag>();
		for (Fields seminar : _courseList) {
			System.out.println(seminar);
			tableRows.add(Tag.containerTag("tr").withChildren(
				Tag.containerTag("td").withText(seminar.firstValueFor(Data.ID)),
				Tag.containerTag("td").withText(seminar.firstValueFor(Data.NAME)),
				Tag.containerTag("td").withText(seminar.firstValueFor(Data.LOCATION)),
				Tag.containerTag("td").withText(seminar.firstValueFor(Data.NUMBER_OF_SEATS)),
				Tag.containerTag("td").withText(seminar.firstValueFor(Data.START_DATE))
			));			
		}
		
		Tag content = Tag.containerTag("table").withAttribute("class", "table table-striped").withChildren(
			Tag.containerTag("thead").withChildren(
				Tag.containerTag("tr").withChildren(
						Tag.containerTag("td").withText("id"),
						Tag.containerTag("td").withText("name"),
						Tag.containerTag("td").withText("location"),
						Tag.containerTag("td").withText("totalSeats"),
						Tag.containerTag("td").withText("start")
				)
			),
			Tag.containerTag("tbody").withChildren(tableRows.toArray(new Tag[]{}))
		);
		

		
		DefaultHtmlTemplate result = new DefaultHtmlTemplate("seminar list", "seminar list", "manage your courses").withContent(content);
		return result.toText();
	}
}
