package prj.view.course;

import ch.gmtech.context.Context;
import ch.gmtech.data.Data;
import ch.gmtech.markup.Tag;
import prj.controller.helper.UrlHelper;
import prj.view.helper.HtmlHelper;
import prj.view.template.DefaultHtmlTemplate;

public class CourseCreateFormView {

	private final Context _context;

	public CourseCreateFormView(Context aContext) {
		_context = aContext;
	}

	public String toText() {
		DefaultHtmlTemplate result = new DefaultHtmlTemplate("seminar", "seminars", "manage your courses")
			.withContent(
						Tag.containerTag("form").withAttribute("class", "form-horizontal")
												.withAttribute("role", "form")
												.withAttribute("method", "post")
												.withAttribute("action", UrlHelper.create(Data.COURSE))
												.withChildren(
							HtmlHelper.inputTextFor(Data.NAME, _context),
							HtmlHelper.inputTextFor(Data.LOCATION, _context),
							HtmlHelper.inputFor("input", Data.NUMBER_OF_SEATS, "number of seats", _context),
							HtmlHelper.inputFor("input", Data.START_DATE, "start date", _context),
							HtmlHelper.inputAreatFor(Data.DESCRIPTION),
							Tag.containerTag("div").withAttribute("class", "form-group").withChildren(
								Tag.containerTag("div").withAttribute("class", "col-sm-1 col-sm-offset-2").withChildren(
									Tag.containerTag("input").withAttribute("type", "submit")
															 .withAttribute("class", "btn btn-primary")
															 .withAttribute("id", "submit")
															 .withAttribute("name", "submit")
															 .withAttribute("value", "send")																
								),
								Tag.containerTag("div").withAttribute("class", "col-sm-1 col-sm-offset-2").withChildren(
										Tag.containerTag("input").withAttribute("type", "reset")
																 .withAttribute("class", "btn btn-warning")
																 .withAttribute("id", "reset")
																 .withAttribute("value", "reset")																
								)
								
							)
						)
		);
		
		return result.toText();
	}
	
}
