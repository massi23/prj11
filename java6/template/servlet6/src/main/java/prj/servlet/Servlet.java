package prj.servlet;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;

import ch.gmtech.context.Context;
import ch.gmtech.context.recogniser.helper.ResourceRecogniser;
import ch.gmtech.data.Data;
import ch.gmtech.logger.AbstractLogger;
import ch.gmtech.logger.PrintStreamLogger;
import ch.gmtech.persistence.AbstractPersistence;
import ch.gmtech.persistence.connection.AbstractDbConnection;
import ch.gmtech.persistence.connection.DbConnectionFactory;
import ch.gmtech.persistence.connection.NullDbConnection;
import ch.gmtech.requesthandler.RequestHandler;
import prj.controller.InternalErrorController;
import prj.controller.ResourceNotFoundController;
import prj.controller.course.CourseCreateController;
import prj.controller.course.CourseCreateFormController;
import prj.controller.course.CourseListController;
import prj.factory.persistence.PersistenceFactory;

public class Servlet extends HttpServlet {
	private static final long serialVersionUID = -9177005934832671692L;
	private DbConnectionFactory _dbConnectionFactory;
	
	@Override
	public void init() throws ServletException {
		super.init();
		try {
			_dbConnectionFactory = new DbConnectionFactory(getInitParameter(Data.ENVIRONMENT));
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	protected void service(HttpServletRequest aRequest, HttpServletResponse aResponse) {
		Context context = new Context(aRequest, aResponse);
		AbstractDbConnection dbConnection = new NullDbConnection();
		try {
			dbConnection = _dbConnectionFactory.create();
			AbstractPersistence coursePersistence = PersistenceFactory.course(dbConnection);
			AbstractLogger logger = new PrintStreamLogger(System.out);
			
			RequestHandler handler = new RequestHandler(logger, new ResourceNotFoundController());
			handler.put(ResourceRecogniser.root(), new CourseListController(coursePersistence));
			handler.put(ResourceRecogniser.list(Data.COURSE), new CourseListController(coursePersistence));
			handler.put(ResourceRecogniser.createForm(Data.COURSE), new CourseCreateFormController());
			handler.put(ResourceRecogniser.create(Data.COURSE), new CourseCreateController(coursePersistence));
			
			handler.handle(context);
			dbConnection.commit();
		} catch(Exception e) {
			context.addError(ExceptionUtils.getStackTrace(e));
			new InternalErrorController().applyOn(context);
		} finally {
			dbConnection.close();
		}
	}
	
}
