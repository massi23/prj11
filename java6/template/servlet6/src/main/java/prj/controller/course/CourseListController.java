package prj.controller.course;

import java.util.List;

import ch.gmtech.context.Context;
import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.AbstractPersistence;
import prj.controller.AbstractController;
import prj.view.course.CourseListView;

public class CourseListController extends AbstractController {

	private final AbstractPersistence _coursePersistence;

	public CourseListController(AbstractPersistence coursePersistence) {
		_coursePersistence = coursePersistence;
	}

	@Override
	public void applyOn(Context aContext) throws Exception {
		List<Fields> courses = _coursePersistence.findAll();
		aContext.writeOut(new CourseListView(courses).toText());
	}

}
