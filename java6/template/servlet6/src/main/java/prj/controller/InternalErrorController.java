package prj.controller;

import javax.servlet.http.HttpServletResponse;

import ch.gmtech.context.Context;

public class InternalErrorController extends AbstractController {

	@Override
	public void applyOn(Context aContext) {
		try {
			StringBuilder errors = new StringBuilder();
			for (String error : aContext.getErrors()) {
				errors.append(error.toString());
			}
			aContext.setResponseCodeTo(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "custom 500 message \n\n" + errors.toString());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
