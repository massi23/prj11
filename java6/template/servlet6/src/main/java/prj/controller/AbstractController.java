package prj.controller;

import ch.gmtech.context.Context;

public abstract class AbstractController {

	public abstract void applyOn(Context aContext) throws Exception;

}
