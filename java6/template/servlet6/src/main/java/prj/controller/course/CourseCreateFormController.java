package prj.controller.course;

import ch.gmtech.context.Context;
import prj.controller.AbstractController;
import prj.view.course.CourseCreateFormView;

public class CourseCreateFormController extends AbstractController {

	@Override
	public void applyOn(Context aContext) throws Exception {
		aContext.writeOut(new CourseCreateFormView(aContext).toText());
	}

}
