package prj.controller.course;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.gmtech.context.Context;
import ch.gmtech.data.Data;
import ch.gmtech.data.DatePatterns;
import ch.gmtech.fields.Fields;
import ch.gmtech.persistence.AbstractPersistence;
import ch.gmtech.resource.validator.ResourceValidator;
import prj.controller.AbstractController;
import prj.controller.helper.UrlHelper;

public class CourseCreateController extends AbstractController {

	private final AbstractPersistence _coursePersistence;

	public CourseCreateController(AbstractPersistence coursePersistence) {
		_coursePersistence = coursePersistence;
		
	}

	@Override
	public void applyOn(Context aContext) throws Exception {
		Fields parameters = aContext.getParameters();
		
		ResourceValidator validator = new ResourceValidator();
		validator.validatePresenceOf(Data.NAME);
		validator.validateMaxLengthOf(Data.NAME, 15);
		validator.validatePresenceOf(Data.LOCATION);
		validator.validateMaxLengthOf(Data.LOCATION, 20);
		validator.validatePresenceOf(Data.NUMBER_OF_SEATS);
		validator.validateNumericalityOf(Data.NUMBER_OF_SEATS);
		validator.validateMaxLengthOf(Data.NUMBER_OF_SEATS, 3);
		validator.validatePresenceOf(Data.START_DATE);
		validator.validateDateFormatOf(Data.START_DATE, DatePatterns.DEFAULT);
		
		if(!validator.isValid(parameters)) {
			aContext.putResourceError(Data.COURSE, validator.errors());
			new CourseCreateFormController().applyOn(aContext);
			return;
		}
		_coursePersistence.save(courseFrom(parameters));
		aContext.redirectTo(UrlHelper.list(Data.COURSE));
	}

	private Fields courseFrom(Fields parameters) throws ParseException {
		String name = parameters.firstValueFor(Data.NAME);
		String description = parameters.firstValueFor(Data.DESCRIPTION);
		String location = parameters.firstValueFor(Data.LOCATION);
		int numberOfSeats = Integer.parseInt(parameters.firstValueFor(Data.NUMBER_OF_SEATS));
		Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse(parameters.firstValueFor(Data.START_DATE));
		
		return Fields.empty()
			.put(Data.NAME, String.valueOf(name))
			.put(Data.NUMBER_OF_SEATS, String.valueOf(numberOfSeats))
			.put(Data.DESCRIPTION, String.valueOf(description))
			.put(Data.LOCATION, String.valueOf(location))
			.put(Data.START_DATE, String.valueOf(startDate))
			;
	}

}
