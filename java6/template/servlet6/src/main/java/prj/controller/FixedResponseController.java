package prj.controller;

import ch.gmtech.context.Context;

public class FixedResponseController extends AbstractController {

	private final String _response;

	public FixedResponseController(String response) {
		_response = response;
	}

	@Override
	public void applyOn(Context aContext) throws Exception {
		aContext.writeOut(_response);
	}

}
