package prj.controller;

import javax.servlet.http.HttpServletResponse;

import ch.gmtech.context.Context;

public class ResourceNotFoundController extends AbstractController {

	@Override
	public void applyOn(Context aContext) throws Exception {
		aContext.setResponseCodeTo(HttpServletResponse.SC_NOT_FOUND, "custom 404 message");
	}

}
