package prj.controller.helper;

public class UrlHelper {

	public static String list(String aResource) {
		return "/" + aResource;
	}

	public static String create(String aResource) {
		return "/" + aResource + "/create";
	}

}
