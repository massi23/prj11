package prj.factory.persistence;

import java.util.Arrays;
import java.util.List;

import ch.gmtech.data.Data;
import ch.gmtech.persistence.AbstractPersistence;
import ch.gmtech.persistence.SqlPersistence;
import ch.gmtech.persistence.connection.AbstractDbConnection;
import prj.data.PeristenceTableData;

public class PersistenceFactory {

	public static AbstractPersistence course(AbstractDbConnection aDbConnection) {
		List<String> columns = Arrays.asList(
				Data.ID, 
				Data.NAME, 
				Data.NUMBER_OF_SEATS, 
				Data.DESCRIPTION,
				Data.LOCATION, 
				Data.START_DATE 
			);
		return new SqlPersistence(aDbConnection, PeristenceTableData.COURSE, columns);
	}

}
