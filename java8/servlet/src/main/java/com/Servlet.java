package com;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.request.HttpRequestHandler;
import com.request.recogniser.ByPathRequestRecogniser;
import com.response.FixedResponse;
import com.response.SeminarResponse;

import ch.gmtech.seminar.DatePatterns;
import ch.gmtech.seminar.Seminar;
import ch.gmtech.seminar.Student;
import ch.gmtech.seminar.report.SeminarCSVReportStrategy;
import ch.gmtech.seminar.report.SeminarHTMLReportStrategy;
import ch.gmtech.seminar.report.SeminarTextReportStrategy;

public class Servlet extends HttpServlet {
	private static final long serialVersionUID = -9177005934832671692L;
	
	private Seminar _seminar;

	@Override
	public void init() throws ServletException {
		super.init();
		try {
			Date startDate = new SimpleDateFormat(DatePatterns.DEFAULT).parse("2016-10-29 20:00:00");
			_seminar = new Seminar("java for dummies", 1, "learn java", "mendrisio", 23, startDate);
			_seminar.addStudent(new Student("Joey", "Ramone"));
			_seminar.addStudent(new Student("Dee Dee", "Ramone"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest aRequest, HttpServletResponse aResponse) throws ServletException, IOException {
		HttpRequestHandler handler = new HttpRequestHandler();
		handler.put(new ByPathRequestRecogniser("/try/me"), new FixedResponse("<h1>you did it!</h1>"));
		handler.put(new ByPathRequestRecogniser("/course/html"), new SeminarResponse(_seminar, "text/html", new SeminarHTMLReportStrategy()));
		handler.put(new ByPathRequestRecogniser("/course/csv"), new SeminarResponse(_seminar, "text/csv", new SeminarCSVReportStrategy()));
		handler.put(new ByPathRequestRecogniser("/course/raw"), new SeminarResponse(_seminar, "text/plain", new SeminarTextReportStrategy()));
		
		handler.handle(aRequest, aResponse);
	}
	
}
