package com.response;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FixedResponse extends AbstractResponse {

	private final String _response;

	public FixedResponse(String response) {
		_response = response;
	}

	@Override
	public void execute(HttpServletRequest aRequest, HttpServletResponse aResponse) throws IOException {
		aResponse.getWriter().write(_response);
	}

}
