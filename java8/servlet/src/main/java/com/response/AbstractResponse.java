package com.response;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractResponse {

	public abstract void execute(HttpServletRequest aRequest, HttpServletResponse aResponse) throws IOException;

}
