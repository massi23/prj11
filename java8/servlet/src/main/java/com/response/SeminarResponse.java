package com.response;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.gmtech.seminar.Seminar;
import ch.gmtech.seminar.report.AbstractSeminarReportStrategy;

public class SeminarResponse  extends AbstractResponse {

	private final Seminar _seminar;
	private final String _contentType;
	private final AbstractSeminarReportStrategy _reportStrategy;

	public SeminarResponse(Seminar seminar, String contentType, AbstractSeminarReportStrategy reportStrategy) {
		_seminar = seminar;
		_contentType = contentType;
		_reportStrategy = reportStrategy;
	}

	@Override
	public void execute(HttpServletRequest aRequest, HttpServletResponse aResponse) throws IOException {
		PrintWriter out = aResponse.getWriter();
		out.write(_seminar.report(_reportStrategy));
		aResponse.setContentType(_contentType);
	}

}
