package com.request.recogniser;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractRequestRecogniser {

	public abstract boolean recognise(HttpServletRequest aRequest);

}
