package com.request.recogniser;

import javax.servlet.http.HttpServletRequest;

public class ByPathRequestRecogniser extends AbstractRequestRecogniser{

	private final String _path;

	public ByPathRequestRecogniser(String path) {
		_path = path;
	}

	@Override
	public boolean recognise(HttpServletRequest aRequest) {
		return aRequest.getRequestURI().equals(_path);
	}

}
