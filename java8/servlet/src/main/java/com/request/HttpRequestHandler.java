package com.request;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.request.recogniser.AbstractRequestRecogniser;
import com.response.AbstractResponse;

public class HttpRequestHandler {

	private final Map<AbstractRequestRecogniser, AbstractResponse> _mappings;
	
	public HttpRequestHandler() {
		_mappings = new HashMap<>();
	}

	public void put(AbstractRequestRecogniser aRecogniser, AbstractResponse anAction) {
		_mappings.put(aRecogniser, anAction);
	}

	public void handle(HttpServletRequest aRequest, HttpServletResponse aResponse) throws IOException {
		for (AbstractRequestRecogniser each : _mappings.keySet()) {
			if(each.recognise(aRequest)) {
				_mappings.get(each).execute(aRequest, aResponse);
				return;
			}
			
		}
	}

}
